jQuery(function ($) {







    $('.js_btn_search_open').click(function(e){
        e.preventDefault()
        $('.js_header__search').addClass('active')
        $('.js_btn_search_field').focus()
    })

    $('.js_btn_search_close').click(function(e){
        e.preventDefault()
        $('.js_header__search').removeClass('active')
    })

    $('.menu__item__submenu__btn').click(function(e){
        e.preventDefault()
        $(this).closest('.js_menu__item__submenu__item').toggleClass('active').find('.menu__item__submenu__item__list').slideToggle('400')
    })




    $('.js_career__page__top a').click(function(e){
        e.preventDefault()
        if (!$(this).hasClass('active')){
            $('.js_career__page__top a').removeClass('active')
            $(this).addClass('active')
            let ind = $(this).index()

            $('.js_career__page__content .career__page__content__item.active').fadeOut(400,function(){
                $(this).removeClass('active')
                $('.js_career__page__content .career__page__content__item').eq(ind).fadeIn(400,function(){
                    $(this).addClass('active')
                })
            });
        }
    })

    $('.js_btn_menu,.js_btn_menu_modal').click(function(e){
        e.preventDefault()
        $('.js_btn_menu').toggleClass('active')
        $('.js_menu_modal').toggleClass('active')
    })

    $('.js_faq_list .faq_item .faq_item__top').click(function(e){
        e.preventDefault()
        $(this).closest('.faq_item').toggleClass('active')
        $(this).closest('.faq_item').find('.faq_item__content').slideToggle(400);
    })






    $(".head__slider").slick({
        infinity: true,
        autoplay: true,
        autoplaySpeed: 2000,
        // arrows: false,
        dots: true,
        prevArrow: $("#head__slider-prev"),
        nextArrow: $("#head__slider-next"),
        appendDots: $(".head__slider_bullets"),
    });

    // $(".calendar__date").slick({
    //     slidesToShow: 5,
    //     dots: false,
    //     prevArrow: $("#calendar-prev"),
    //     nextArrow: $("#calendar-next"),
    //     responsive: [
    //         {
    //             breakpoint: 768,
    //             settings: {
    //                 slidesToShow: 3,
    //             },
    //         },
    //         {
    //             breakpoint: 480,
    //             settings: {
    //                 slidesToShow: 1,
    //             },
    //         },
    //     ],
    // });
    $(".calendar__items").slick({
        slidesToShow: 5,
        dots: false,
        prevArrow: $("#calendar-prev"),
        nextArrow: $("#calendar-next"),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                },
            },
        ],
    });

    $('.photos__slider').slick({
        // arrows:false,
        dots:false,
        infinity: true,
        slidesToShow:3,
        centerMode: true,
        prevArrow: $('#photos__prev'),
        nextArrow: $('#photos__next'),
        responsive: [
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
              }
            }
          ]

    });

    $("#burger").on("click", function (e) {
        e.preventDefault();
        $(this).toggleClass("active");
        $(".menu").toggleClass("active");
        $("body").toggleClass("lock");
    });

    $(".icon__submenu").on("click", function () {
        $(this).closest(".dropdown").toggleClass("active");
    });


    $('#form').on('submit', function(e){
        let html = '<div class="result"><i class="icon icon__success"></i><div class="head2__title">Thank You!</div><p>Your message has been sent successfully!</p><a href="/" class="yellowbtn">Go to main page</a></div>';
        $(this).parent().html(html);
    });


    $('.storys__sliders').slick({
        arrows:false,
        dots:true
    });
    $('.slider__items').slick({
        arrows:false,
        dots:true
    });

    $('.career__btn').on('click', function(e){
        e.preventDefault();
        $('.career__btn').not(this).removeClass('active');
        $(this).addClass('active');
        let id = $(this).attr('href');
        $(id).addClass('active');
        $('.career__tab').not(id).removeClass('active');
    });

    $('.spoiler__btn').on('click', function(e){
        e.preventDefault();
        $(this).parent().toggleClass('active');
    });

    $('.cancelbtn').on('click', function(e){
        $.magnificPopup.close();
    });


    function showHidePassBtn(){
        $('input[type="password"]').wrap('<div class="rmpass"></div>');
        $('.rmpass').append('<button class="rmpass__btn"></button>');
        $('.rmpass__btn').on('click', function(e){
            e.preventDefault();
            let pass = $(this).closest('.rmpass').find('input');
            if(pass.attr('type')=='password'){
                pass.attr('type', 'textbox');
                $(this).addClass('active');
            }else{
                pass.attr('type', 'password');
                $(this).removeClass('active');
            }
        });
    }
    showHidePassBtn();


});
